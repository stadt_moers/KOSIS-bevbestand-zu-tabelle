# KOSIS-Bestand-zu-Tabelle

Ein R-Script das KOSIS-Bestandsdaten in Tabellendaten, wie CSV oder Exceldateien
umwandelt.

Als KOSISBestand.txt existiert ein Beispieldatensatz nach der Definition
des KOSIS-Arbeitsgemeinschaft HHSTAT aus dem Jahr 2019 und der Ergebnisdatensatz
ist als "data.xlsx" zu finden.

Weitere Informationen zum Datensatz zum Bevölkerungsbestand finden sich unter:
https://www.staedtestatistik.de/arbeitsgemeinschaften/hhstat/datensaetze
